package com.adamkorzeniak.ml.newman.domain;

import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class Simulation {

    //Environment

    private final int populationLimit;
    private final int generationLimit;

    private int currentGeneration = 0;

    private List<Double> averageFitness;
    private List<Double> bestFitness;

    public static Simulation sample() {
        return new Simulation(100, 10);
    }

    public void start() {

    }

    public boolean hasNextGeneration() {
        return currentGeneration < generationLimit;
    }

    public final Optional<Double> getOverallFitnessImprovement(int lastGenerations) {
        return getFitnessImprovement(lastGenerations, averageFitness);
    }

    public final Optional<Double> getBestFitnessImprovement(int lastGenerations) {
        return getFitnessImprovement(lastGenerations, bestFitness);
    }

    private Optional<Double> getFitnessImprovement(int lastGenerations, List<Double> overallFitnesses) {
        if (currentGeneration < lastGenerations) {
            return Optional.empty();
        }
        double currentFitness = overallFitnesses.get(currentGeneration);
        double previousFitness = overallFitnesses.get(currentGeneration - lastGenerations);
        return Optional.of(currentFitness / previousFitness);
    }

    private void storeGenerationSummary() {
        averageFitness.add(environment.getOverallFitness().orElse(null));
        bestFitness.add(environment.getBestFitness().orElse(null));
    }
}
